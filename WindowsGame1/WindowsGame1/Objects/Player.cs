﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    public class Player : GameObject
    {
        public MoveManager mm;
        private Point LastPosition;

        public Player(Game1 game, Point position, Point size)
        {
            base.Game = game;
            base.Position = position;
            base.Size = size;
            mm = new MoveManager(this);
            LastPosition = Position;

        }

        public void LoadResources()
        {
            base.SetTexture(Game.LoadTexture("./Data/Player/22.png"));
            base.AddAnimation("Idle", new Point(0, 0), new Point(80, 44), 12);
            base.AddAnimation("Run", new Point(0, 44), new Point(69, 52), 12);
        }



        public override void Update(GameTime gameTime)
        {
            mm.Update(gameTime);

            base.Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {


            base.Draw(gameTime);
        }
    }
}
