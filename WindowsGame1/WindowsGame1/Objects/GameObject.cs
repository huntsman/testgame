﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    public class GameObject
    {
        public int ID;
        public int MapID;
        public string Name;
        public Point Position;
        public float Rotation;
        public Point Size;
        List<Sprite> Sprites;
        Texture2D Texture;
        Rectangle screenRect;
        public Game1 Game;

        public GameObject(/*Game1 game*/)
        {
            Sprites = new List<Sprite>();
            //this.Game = game;
        }

    #region #Анимация

        public void StartAnimation(string name)
        {
            foreach (Sprite spr in Sprites)
            {
                if (spr.Name == name)
                    spr.FramePause = false;
                else
                    spr.FramePause = true;
            }
        }

        public void StopAnimation()
        {
            foreach (Sprite spr in Sprites)
            {
                    spr.FramePause = true;
            }
        }

        public void AddAnimation(string name, Point xy, Point Size, int count)
        {
            Sprite spr = new Sprite(name, xy, Size, count);
            Sprites.Add(spr);
        }

        public void DeleteAnimation(string name)
        {
            foreach (Sprite spr in Sprites)
            {
                if (spr.Name == name)
                {
                    Sprites.Remove(spr);
                    break;
                }
            }
        }


    #endregion

        public void SetTexture(Texture2D texture)
        {
            Texture = texture;
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (Sprite spr in Sprites)
            {
                if (!spr.FramePause)
                    if (spr.FrameCount > 1)
                    {
                        spr.Update(gameTime);
                        break;
                    }
            }

            screenRect = new Rectangle(Position.X, Position.Y, Size.X, Size.Y);
        }


        public virtual void Draw(GameTime gameTime)
        {
            SpriteEffects effects = SpriteEffects.None;
            foreach (Sprite spr in Sprites)
            {
                if (!spr.FramePause)
                    if (spr.FrameCount > 1)
                    {
                        Game.spriteBatch.Draw(Texture, screenRect, spr.Draw(), Color.White, Rotation, new Vector2(screenRect.Width / 2, screenRect.Height/2), effects, 0);
                        break;
                    }
                    else
                    {
                        Game.spriteBatch.Draw(Texture, screenRect, Color.White);
                        break;
                    }
            }

        }


    }
}
