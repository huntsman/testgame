﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace WindowsGame1
{
    public class Input
    {
        Game1 Game;
        KeyboardState oldState;
        public Input(Game1 game)
        {
            this.Game = game;
        }


        MouseState oldmState;
        Point ktemp;
        public void Update(GameTime gameTime)
        {
            MouseState mState = Mouse.GetState();
            KeyboardState keyState = Keyboard.GetState();
            ktemp = Game.Map.viewer.Shift;

            if (keyState.IsKeyDown(Keys.D))
            {
                ktemp = new Point(ktemp.X + 1, ktemp.Y);
            }
            if (keyState.IsKeyDown(Keys.A))
            {
                ktemp = new Point(ktemp.X - 1, ktemp.Y);
            }

            Game.Map.viewer.Shift = ktemp;

            if (mState.LeftButton ==  ButtonState.Pressed)
                if (oldmState.LeftButton != ButtonState.Pressed)
                {
                    Game.GamePlayer.mm.MoveTo(new Point(mState.X, mState.Y));
                    Game.GamePlayer.StartAnimation("Run");
                }



            if (Game.GamePlayer.mm.CountMovies() == 0)
                Game.GamePlayer.StartAnimation("Idle");





            oldState = keyState;
            oldmState = mState;
        }


    }
}
