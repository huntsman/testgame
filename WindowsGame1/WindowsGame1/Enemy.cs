﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace WindowsGame1
{
    class Enemy : GameObject
    {
        public bool RandomMove;
        public MoveManager mm;
        Random Randomizer;
        Point SavePos;
        public Enemy(Game1 game, Point position, Point size)
        {
            base.Game = game;
            base.Position = position;
            base.Size = size;
            Randomizer = new Random();
            SavePos = Position;
            mm = new MoveManager(this);

        }


        public void LoadResources()
        {
            base.ID = 1;
            base.Name = "ZOmbie";
            base.SetTexture(Game.LoadTexture("./Data/Enemies/Zombie/Idle.png"));
            base.AddAnimation("Idle", new Point(0, 0), new Point(50, 57), 36);
        }





        int timeElapsed;
        int Speed = 3000;
        bool SpeedTimer(GameTime gameTime)
        {
            timeElapsed += gameTime.ElapsedGameTime.Milliseconds;


            if (timeElapsed > Speed)
            {
                timeElapsed = 0;
                return true;
            }

            return false;
        }

        public override void Update(GameTime gameTime)
        {

            if (RandomMove)
            {
                if (SpeedTimer(gameTime))
                {
                    if (mm.CountMovies() == 0)
                    {
                        int x = Randomizer.Next(SavePos.X, SavePos.X + Randomizer.Next(20));
                        int y = Randomizer.Next(SavePos.Y, SavePos.Y + Randomizer.Next(20));
                        mm.MoveTo(new Point(x,y));
                    }
                }

            }

            mm.Update(gameTime);
            base.Update(gameTime);
        }
    }
}
