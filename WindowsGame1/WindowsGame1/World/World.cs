﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    class World
    {
        Game1 Game;
        List<Map> Maps;
        int CurrentMap;
        bool StartWorld;
        public World(Game1 game)
        {
            Game = game;
            Maps = new List<Map>();
            LoadMaps();
        }

        void LoadMaps()
        {
            Map m = new Map(Game);
            m.MapID = 0;

        }


        public void LoadPlayerWorld()
        {
            CurrentMap = Game.GamePlayer.MapID;
            StartWorld = true;
        }


        public void Update(GameTime gameTime)
        {
            if (StartWorld)
            Maps[CurrentMap].Update(gameTime);
        }

        public void DrawLine1()
        {
            if (StartWorld)
            foreach (MapSprite m in Maps[CurrentMap].Line1)
            {
                Draw(m);
            }
        }

        public void DrawLine2()
        {
            if (StartWorld)
            foreach (MapSprite m in Maps[CurrentMap].Line2)
            {
                Draw(m);
            }
        }

        void Draw(MapSprite spr)
        {
            SpriteEffects effects = SpriteEffects.None;
            Rectangle screenRect = new Rectangle(spr.Location.X, spr.Location.Y, spr.Size.X, spr.Size.Y);
            if (!spr.sprite.FramePause)
                if (spr.sprite.FrameCount > 1)
                {
                    Game.spriteBatch.Draw(Maps[CurrentMap].GlobalTexture, screenRect, spr.sprite.Draw(), Color.White, spr.Rotation, new Vector2(screenRect.Width / 2, screenRect.Height / 2), effects, 0);
                }
                else
                {
                    Game.spriteBatch.Draw(Maps[CurrentMap].GlobalTexture, screenRect, Color.White);
                }
        }
    }
}
