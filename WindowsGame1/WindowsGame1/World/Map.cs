﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    class Map
    {
        public int MapID;
        Game1 Game;
        public List<MapSprite> Line1;
        public List<Rectangle> CollisionLine;
        public List<MapSprite> Line2;
        public Texture2D GlobalTexture;
        public Point Size;

        public Map(Game1 game)
        {
            Game = game;
            Line1 = new List<MapSprite>();
            CollisionLine = new List<Rectangle>();
            Line2 = new List<MapSprite>();
        }

        public void Update(GameTime gameTime)
        {
            foreach (MapSprite ms in Line1)
            {
                ms.sprite.Update(gameTime);
            }
            foreach (MapSprite ms in Line2)
            {
                ms.sprite.Update(gameTime);
            }
        }

    }

    struct MapSprite
    {
        public Sprite sprite;
        public Point Size;
        public float Rotation;
        public Point Location;
    }
}
