﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace WindowsGame1
{
    public class MoveManager
    {
        GameObject GO;
        bool ismove;
        float iSpeed;
        List<Point> Movies;
        public MoveManager(GameObject go)
        {
            Movies = new List<Point>();
            GO = go;
            iSpeed = 4;
        }


        public float Speed
        {
            set
            {
                iSpeed = value;
            }
            get
            {
                return iSpeed;
            }

        }

        int TimerSpeed = 5;
        int timeElapsed;
        bool SpeedTimer(GameTime gameTime)
        {
            timeElapsed += gameTime.ElapsedGameTime.Milliseconds;


            if (timeElapsed > TimerSpeed)
            {
                timeElapsed = 0;
                return true;
            }

            return false;
        }

        public int CountMovies()
        {
            return Movies.Count;
        }

        public void MoveTo(Point xy)
        {
            ClearMovies();
            Movies.Add(xy);


            ismove = true;
        }

        void AddMove(Point xy)
        {
            Movies.Add(xy);
        }

        void ClearMovies()
        {
            ismove = false;
            Movies.RemoveRange(0, Movies.Count);
        }

        float fOstDiffX = 0;
        float fOstDiffY = 0;
        public void Update(GameTime gameTime)
        {

            if (ismove)
            {

                if (SpeedTimer(gameTime))
                {
                    // Get a Vector between object A and Target
                    double _nx = Movies[0].X - GO.Position.X;
                    double _ny = Movies[0].Y - GO.Position.Y;
                    GO.Rotation = (float)Math.Atan2(_ny, _nx);
                    // Get Distance
                    double _distance = Math.Sqrt((_nx * _nx) + (_ny * _ny));

                    if (_distance > iSpeed)
                    {
                        // Normalize vector (make it length of 1.0)
                        double _vx = _nx / _distance;
                        double _vy = _ny / _distance;

                        // Move object based on vector and speed
                        fOstDiffX += (float)_vx * iSpeed;
                        fOstDiffY += (float)_vy * iSpeed;


                        GO.Position.X += (int)fOstDiffX;

                        if (fOstDiffX >= 1.0)
                            fOstDiffX = 0;

                        if (fOstDiffX <= -1.0)
                            fOstDiffX = 0;

                        GO.Position.Y += (int)fOstDiffY;

                        if (fOstDiffY >= 1.0)
                            fOstDiffY = 0;

                        if (fOstDiffY <= -1.0)
                            fOstDiffY = 0;


                    }
                    else
                    {
                        Movies.RemoveAt(0);
                        fOstDiffX = 0;
                        fOstDiffY = 0;
                        //// Destination Arrived
                        //GO.Position.X = NewPosition.X;
                        //GO.Position.Y = NewPosition.Y;

                    }
                }

                if (Movies.Count == 0)
                    ismove = false;
            }

        }

    }
}
