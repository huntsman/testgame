using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Squared.Tiled;
using System.IO;

namespace TiledExample {
    public class TiledExample : Microsoft.Xna.Framework.Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Map map;
        Vector2 viewportPosition;

        public TiledExample () {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize () {
            base.Initialize();
        }

        protected override void LoadContent () {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            map = Map.Load(Path.Combine(Content.RootDirectory, "111.tmx"), Content);
        }

        protected override void UnloadContent () {
        }

        protected override void Update (GameTime gameTime) {

            KeyboardState ks = Keyboard.GetState();
            float scrollSpeed = 16.0f;
            if (ks.IsKeyDown(Keys.Left))
            {
                viewportPosition.X -= 1 * scrollSpeed;
            }
            if (ks.IsKeyDown(Keys.Right))
            {
                viewportPosition.X += 1 * scrollSpeed;
            }
            if (ks.IsKeyDown(Keys.Up))
            {
                viewportPosition.Y -= 1 * scrollSpeed;
            }
            if (ks.IsKeyDown(Keys.Down))
            {
                viewportPosition.Y += 1 * scrollSpeed;
            }
            
            

            base.Update(gameTime);
        }

        protected override void Draw (GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);

            spriteBatch.Begin();
            map.Draw(spriteBatch, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), viewportPosition);
            spriteBatch.End();
        }
    }
}
