﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;


namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        public Player GamePlayer;
        List<GameObject> Objects;
        List<Enemy> Enemis;
        List<World> Worlds;
        public TiledMap.Map Map;
        Input input;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            IsMouseVisible = true;
            //graphics.IsFullScreen = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Enemis = new List<Enemy>();
            input = new Input(this);
            DrawHelper.Game = this;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
             //Create a new SpriteBatch, which can be used to draw textures.
            Enemy en = new Enemy(this, new Point(280, 50), new Point(50,57));
            en.LoadResources();
            en.StartAnimation("Idle");
            en.RandomMove = true;
            Enemis.Add(en);


            GamePlayer = new Player(this, new Point(50, 50), new Point(69, 52));
            GamePlayer.LoadResources();
            GamePlayer.StartAnimation("Idle");

            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("SpriteFont1");

            // TODO: use this.Content to load your game content here
            Map = new TiledMap.Map();
        }

        public Texture2D LoadTexture(string path)
        {
            Texture2D acttext = Texture2D.FromStream(GraphicsDevice, File.OpenRead(path));
            return acttext;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            // TODO: Add your update logic here
            foreach (Enemy en in Enemis)
            {
                en.Update(gameTime);
            }

            GamePlayer.Update(gameTime);

            input.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            Map.Draw(spriteBatch);


            foreach (Enemy en in Enemis)
            {
                en.Draw(gameTime);
            }

            GamePlayer.Draw(gameTime);


            try
            {

                spriteBatch.DrawString(spriteFont, (1000 / gameTime.ElapsedGameTime.Milliseconds).ToString(), new Vector2(10, 10), Color.DeepSkyBlue);

            }

            catch (DivideByZeroException) { } 

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
