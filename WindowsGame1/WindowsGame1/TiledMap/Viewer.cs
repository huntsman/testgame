﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;


namespace TiledMap
{
    public class Viewer
    {

        public Point ViewPoint;
        public int Width, Height;

        public Viewer(int Width, int Height)
        {
            this.Width = Width;
            this.Height = Height;
            vchunk = new VChunk[Width, Height];
        }

        public VChunk[,] vchunk;

        private Point shift = new Point();

        public Point Shift
        {
            get { return shift; }
            set
            {
                shift = value;
                if (shift.X > (MapHelper.ChunkSize.X))
                {
                    shift.X = 0;
                    ShiftX(-1);
                }

                if (shift.X < (-(MapHelper.ChunkSize.X )))
                {
                    shift.X = 0;
                    ShiftX(1);
                }


                if (ViewPoint.X < 0)
                    ViewPoint.X = 0;
            }
        }

        void ShiftX(int x)
        {
            for (int i = 0; i < Height; i++)
            {
                for (int n = 0; n < Width; n++)
                {
                    vchunk[n, i].Index.X += x;
                }
            }
        }


        public void AddToViewer()
        {

        }


        //public void SetViewPoint(Point point)
        //{

        //    ViewPoint.X = point.X / MapHelper.ChunkSize.X;
        //    ViewPoint.Y = point.Y / MapHelper.ChunkSize.Y;

        //}


    }

    public class VChunk
    {
        public Point Index;
        //public Point LocPosit;
        //private Point Shift;
        public Point Position;

        //public VChunk()
        //{
        //    Shift = position;
        //}

        //public Point Position
        //{
        //    get { return Shift; }
        //    set { position = Shift = value; }
        //}

        //public void AddToShiftX(int x)
        //{
        //    Shift.X += x;
        //}
    }
}
