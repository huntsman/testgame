﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TiledMap
{
    class Zone
    {
        public string Name;
        public Texture2D MapImage;
        public Chunk[,] Chunks;


        public Zone(string name, Texture2D image)
        {
            Name = name;
            MapImage = image;
        }


    }
}
