﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace TiledMap
{
    public class Map
    {
        public Viewer viewer;
        ZoneGenerator ZG;
        Zone zone;
        int ii, nn;
        Point p;
        public bool GridView;
        public Map()
        {
            viewer = new Viewer(10, 10);
            screenRect.Width = MapHelper.ChunkSize.X;
            screenRect.Height = MapHelper.ChunkSize.Y;

            ZG = new ZoneGenerator();
            zone = ZG.Generate();

            //Заполняем Viewer
            ii = nn = 0;
            for (int i = 0; i < viewer.Height; i++)
            {
                for (int n = 0; n < viewer.Width; n++)
                {
                    p.X = nn * MapHelper.ChunkSize.X + /*sdv.X +*/ viewer.Shift.X;
                    p.Y = ii * MapHelper.ChunkSize.Y + /*sdv.Y +*/ viewer.Shift.Y;

                    viewer.vchunk[nn, ii] = new VChunk();
                    viewer.vchunk[nn, ii].Index = new Point(n, i);
                    viewer.vchunk[nn, ii].Position = p;

                    //DrawHelper.monitor.Draw(zone.MapImage, p, zone.Chunks[n, i].ImageBlock);
                    nn += 1;
                }

                nn = 0;
                ii += 1;
            }


        }

        Rectangle screenRect = new Rectangle();
        public void Draw(SpriteBatch sb)
        {

            for (int i = 0; i < viewer.Height; i++)
            {
                for (int n = 0; n < viewer.Width; n++)
                {
                    screenRect.X = viewer.Shift.X + viewer.vchunk[n, i].Position.X;
                    screenRect.Y = viewer.Shift.Y + viewer.vchunk[n, i].Position.Y;

                    //p.X = 
                    //p.Y = 
                    //DrawHelper.monitor.Draw(zone.MapImage, p, zone.Chunks[viewer.vchunk[n, i].Index.X, viewer.vchunk[n, i].Index.Y].ImageBlock);
                    sb.Draw(zone.MapImage, screenRect, zone.Chunks[viewer.vchunk[n, i].Index.X, viewer.vchunk[n, i].Index.Y].ImageBlock, Color.White);

                }


            }



            if (GridView)
                ViewGrid(sb);
        }




        //Pen blackPe = new Pen(Color.Black, 1);
        //Point ts = new Point((int)(MapHelper.ChunkSize.X * 1.34), (int)(MapHelper.ChunkSize.Y * 1.34));

        void ViewGrid(SpriteBatch sb)
        {
            



            //int xpr = 0;
            //int ypr = 0;
            
            //for (int i = 0; i < viewer.Width; i++)
            //{
            //    for (int n = 0; n < viewer.Height; n++)
            //    {
            //        DrawHelper.monitor.DrawRectangle(blackPe, xpr /*+ sdv.X*/, ypr /*+ sdv.Y*/, ts.X, ts.Y);

            //        ypr += ts.Y + 1;
            //    }
            //    xpr += ts.X + 1;
            //    ypr = 0;
            //}
        }




    }
}
