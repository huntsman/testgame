﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WindowsGame1;

namespace TiledMap
{
    class ZoneGenerator
    {

        public Zone Generate()
        {
            Zone tmap = new Zone("TestMap", DrawHelper.Game.LoadTexture("./tds.png"));
            int W, H;
            W = tmap.MapImage.Width / MapHelper.ChunkSize.X;
            H = tmap.MapImage.Height / MapHelper.ChunkSize.Y;

            tmap.Chunks = new Chunk[W, H];

            Point p = new Point();
            for (int i = 0; i < H; i++)
            {
                for (int n = 0; n < W; n++)
                {
                    tmap.Chunks[n, i] = new Chunk();
                    tmap.Chunks[n, i].ImageBlock = new Rectangle(p.X, p.Y, MapHelper.ChunkSize.X, MapHelper.ChunkSize.Y);
                    p.X += MapHelper.ChunkSize.X;
                }

                p.X = 0;
                p.Y += MapHelper.ChunkSize.Y;
            }

            return tmap;
        }
    }
}
