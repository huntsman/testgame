﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SceneEditor
{
    class Sprite
    {
        public string Name;
        Point Coordinates;
        Point FrameSize;
        public int FrameCount;
        public bool FramePause = true;
        Rectangle r;
        public Sprite(string name, Point xy, Point Size, int count)
        {
            Name = name;
            Coordinates = xy;
            FrameCount = count;
            FrameSize = Size;
        }

        int currentFrame;
        int timeElapsed;
        int timeForFrame = 100;

        public void Pause(bool p)
        {
            this.FramePause = p;
            r = new Rectangle();
        }

        public void Update(GameTime gameTime)
        {
                timeElapsed += gameTime.ElapsedGameTime.Milliseconds;
                int tempTime = timeForFrame;

                if (timeElapsed > tempTime)
                {
                    currentFrame = (currentFrame + 1) % FrameCount;
                    timeElapsed = 0;
                }


                r.X = currentFrame * FrameSize.X;
                r.Y = Coordinates.Y;
                r.Width = FrameSize.X;
                r.Height = FrameSize.Y;
                //r = new Rectangle(currentFrame * FrameSize.X, Coordinates.Y, FrameSize.X, FrameSize.Y);

        }

        public Rectangle Draw()
        {
            return r;
        }
    }
}
