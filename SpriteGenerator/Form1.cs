﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SpriteGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Image tt = Image.FromFile(textBox1.Text + "/" + textBox2.Text + "0" + ".png");
            int rr = int.Parse(textBox3.Text);
            Point size = new Point(tt.Width, tt.Height);
            Bitmap bm = new Bitmap(size.X * rr, size.Y);
            Graphics graph = Graphics.FromImage(bm);
            int hcord = 0;
            for (int i = 0; i < rr; i++)
            {
                tt = Image.FromFile(textBox1.Text + "/" + textBox2.Text + i + ".png");
                graph.DrawImage(tt, hcord,0);
                hcord += size.X;
                
            }

            //tt = bm;
            //FileStream fs = File.Create("./Sprite.png");
            bm.Save("./Sprite.png");
        }
    }
}
