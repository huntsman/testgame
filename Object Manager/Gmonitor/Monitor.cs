﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ObjectManager
{
    public partial class Monitor : UserControl
    {
        public MapMode Mode;

        Bitmap bm;
        Graphics graf;
        public Point MSize
        {
            get
            {
                return (Point)pictureBox1.Size;
            }
        }
        public Monitor()
        {
            InitializeComponent();




        }

        public void Initialize()
        {
            bm = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            graf = Graphics.FromImage(bm);
        }


        //void Draw()
        //{
        //}
        //Enum DrawMode
        //{ 
        //    DrawImage
        //}


        public void DrawImage(Image img, Point point)
        {
            graf.DrawImage(img, point.X, point.Y, img.Width, img.Height);
            pictureBox1.Image = bm;
        }

        Rectangle rtemp = new Rectangle();
        public void DrawImage(Image img, Point point, Rectangle sea)
        {
            rtemp.X = point.X;
            rtemp.Y = point.Y;
            rtemp.Width = MapHelper.ChunkSize.X;
            rtemp.Height = MapHelper.ChunkSize.Y;

            graf.DrawImage(img, rtemp, sea, GraphicsUnit.Pixel);
            pictureBox1.Image = bm;
        }

        public void DrawRectangle(Pen pen, int x, int y, int w, int h)
        {
            graf.DrawRectangle(pen, x, y, w, h);
            pictureBox1.Image = bm;
        }


        public void DrawText(string Text)
        {
            Font f = new Font("Arial",10f);
            //f.Size = 10f;
            //f.Name = "Arial";
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            graf.DrawString(Text, f, drawBrush, new PointF());
        }

        public void Clear()
        {

            graf.Clear(Color.SkyBlue);
            
        }


        public void Refresh()
        {
            pictureBox1.Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //Point p = (Point)sender;

        }




        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {

            switch (Mode)
            {
                case MapMode.MOVE:
                    {
                        MoveMap(e);
                        break;
                    }
                case MapMode.SELECT:
                    {
                        Selector(e);
                        break;
                    }
            }

        }


        Point tmp = new Point();
        void MoveMap(MouseEventArgs e)
        {
            if (LeftMouseDown)
            {
                tmp = BaseHelper.Game.map.viewer.Shift;

                int ttx = (e.X - MousePoint.X);
                int tty = (e.Y - MousePoint.Y);

                if (ttx > 5)
                {
                    tmp.X += 8;
                    MousePoint.X += ttx;
                }
                else if (ttx < (-5))
                {
                    tmp.X -= 8;
                    MousePoint.X += ttx;
                }

                if (tty > 5)
                {
                    tmp.Y += 8;
                    MousePoint.Y += tty;
                }
                else if (tty < (-5))
                {
                    tmp.Y -= 8;
                    MousePoint.Y += tty;
                }

                


                BaseHelper.Game.map.viewer.Shift = tmp;
                //MousePoint = e.Location;
            }
        }

        void Selector(MouseEventArgs e)
        {
            for (int i = 0; i < BaseHelper.Game.map.viewer.Height; i++)
            {
                for (int n = 0; n < BaseHelper.Game.map.viewer.Width; n++)
                {
                    if (BaseHelper.Game.map.viewer.vchunk[n, i].Position.X < e.X & e.X < BaseHelper.Game.map.viewer.vchunk[n, i].Position.X + MapHelper.ChunkSize.X)
                        if (BaseHelper.Game.map.viewer.vchunk[n, i].Position.Y < e.Y & e.Y < BaseHelper.Game.map.viewer.vchunk[n, i].Position.Y + MapHelper.ChunkSize.Y)
                        {
                            BaseHelper.Game.map.viewer.Selected.X = n;
                            BaseHelper.Game.map.viewer.Selected.Y = i;
                            return;
                        }

                }
            }
        }

        bool LeftMouseDown = false;
        bool CreateRect = false;
        Point MousePoint;
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                LeftMouseDown = true;
                MousePoint = e.Location;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                LeftMouseDown = false;
                if (CreateRect)
                {
                }

                if (Mode == MapMode.SELECT)
                    if (BaseHelper.SelectedTile.Length > 0)
                    {
                        BaseHelper.Game.map.AddNewTile();
                    }
            }
        } 
    }

    public enum MapMode
    {
        NONE,
        SELECT,
        MOVE
    }

}
