﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace ObjectManager
{
    public static class DrawHelper
    {
        public static Monitor monitor;

        public static void Draw(Image img, Rectangle rect)
        {
            if (rect.Height != 0 & rect.Width != 0)
            if (img.Height != rect.Height | img.Width != rect.Width)
                img = DrawHelper.ResizeImg(img, rect.Width, rect.Height);

            monitor.DrawImage(img, new Point(rect.X, rect.Y));
        }

        public static Image ResizeImg(Image b, int nWidth, int nHeight)
        {
            Image result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.DrawImage(b, 0, 0, nWidth, nHeight);
                g.Dispose();
            }
            return result;
        }

    }
}
