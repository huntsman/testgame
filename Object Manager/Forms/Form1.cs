﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ObjectManager
{
    public partial class Form1 : Form
    {

        ModelBox modelbox;
        public Form1()
        {
            InitializeComponent();
            
            MapHelper.ChunkSize = new Point(64,64);
            modelbox = new ModelBox(monitor1);
            BaseHelper.Game = modelbox;

            LoadRes();
            
        }

        void LoadRes()
        {
            ResHelper.SelectedTile = Image.FromFile("./SelectedTile.png");
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //MapHelper.MapSize = new Point(50, 50);
            //tiledExplorer1.LoadTile("./222.png");
            ////spriteFontControl1.CreateGrid(64, 64);

            //if (modelbox.map.GridView)
            //    modelbox.map.GridView = false;
            //else
            //    modelbox.map.GridView = true;

            //DrawHelper.monitor.Mode = MapMode.SELECT;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //tiledExplorer1.gr

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {

        }

        Point ktemp;
        private void button2_KeyDown(object sender, KeyEventArgs e)
        {
            ktemp = modelbox.map.viewer.Shift;

            if (e.KeyCode == Keys.D)
                ktemp = new Point(ktemp.X + 5, ktemp.Y);

            if (e.KeyCode == Keys.A)
                ktemp = new Point(ktemp.X - 5, ktemp.Y);

            if (e.KeyCode == Keys.W)
                ktemp = new Point(ktemp.X, ktemp.Y - 5);

            if (e.KeyCode == Keys.S)
                ktemp = new Point(ktemp.X, ktemp.Y + 5);


            modelbox.map.viewer.Shift = ktemp;
        }



        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (BaseHelper.Game.map.viewer.Selected.X != -1)
            {
                BaseHelper.Game.map.viewer.Selected.X = -1;
                BaseHelper.Game.map.viewer.Selected.Y = -1;
            }
        }

        private void показыватьСеткуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmb = (ToolStripMenuItem)sender;

            if (!tsmb.Checked)
            {
                tsmb.Checked = true;
                modelbox.map.GridView = true;
            }
            else
            {
                tsmb.Checked = false;
                modelbox.map.GridView = false;
            }


        }

        private void создатьНаборТайтловToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            if (od.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tiledExplorer1.AddTile(od.FileName, od.SafeFileName);
            }
        }

        private void NewMap_Click(object sender, EventArgs e)
        {

        }

        private void Stamp_Click(object sender, EventArgs e)
        {
            DrawHelper.monitor.Mode = MapMode.SELECT;

            CheckButton(((ToolStripButton)sender).Name);
            ((ToolStripButton)sender).Checked = true;
        }



        private void Move_Click(object sender, EventArgs e)
        {
            DrawHelper.monitor.Mode = MapMode.MOVE;

            CheckButton(((ToolStripButton)sender).Name);
            ((ToolStripButton)sender).Checked = true;
        }


        void CheckButton(string name)
        {
            for (int i = 4; i < toolStrip1.Items.Count; i++)
            {
                if (((ToolStripButton)toolStrip1.Items[i]).Name != name)
                    ((ToolStripButton)toolStrip1.Items[i]).Checked = false;
            }
        }

    }
}
