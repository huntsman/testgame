﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ObjectManager.Forms
{
    public partial class NewMap : Form
    {
        public string Name;
        public Point ChunkSize;
        public Point MapSize;

        public NewMap()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            Name = textBox1.Text;

            ChunkSize = new Point();
            ChunkSize.X = int.Parse(textBox2.Text);
            ChunkSize.Y = int.Parse(textBox3.Text);

            MapSize = new Point();
            MapSize.X = int.Parse(textBox4.Text);
            MapSize.Y = int.Parse(textBox5.Text);

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

    }
}
