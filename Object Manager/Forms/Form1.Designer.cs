﻿namespace ObjectManager
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьКакToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.показыватьСеткуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.картаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьНаборТайтловToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.NewMap = new System.Windows.Forms.ToolStripButton();
            this.OpenMap = new System.Windows.Forms.ToolStripButton();
            this.SaveMap = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Stamp = new System.Windows.Forms.ToolStripButton();
            this.Fill = new System.Windows.Forms.ToolStripButton();
            this.Choose = new System.Windows.Forms.ToolStripButton();
            this.Move = new System.Windows.Forms.ToolStripButton();
            this.Delete = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.layers1 = new ObjectManager.Layers();
            this.tiledExplorer1 = new ObjectManager.TiledExplorer();
            this.monitor1 = new ObjectManager.Monitor();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.видToolStripMenuItem,
            this.картаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(870, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.сохранитьКакToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            // 
            // сохранитьКакToolStripMenuItem
            // 
            this.сохранитьКакToolStripMenuItem.Name = "сохранитьКакToolStripMenuItem";
            this.сохранитьКакToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.сохранитьКакToolStripMenuItem.Text = "Сохранить как..";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            // 
            // видToolStripMenuItem
            // 
            this.видToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.показыватьСеткуToolStripMenuItem});
            this.видToolStripMenuItem.Name = "видToolStripMenuItem";
            this.видToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.видToolStripMenuItem.Text = "Вид";
            // 
            // показыватьСеткуToolStripMenuItem
            // 
            this.показыватьСеткуToolStripMenuItem.Name = "показыватьСеткуToolStripMenuItem";
            this.показыватьСеткуToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.показыватьСеткуToolStripMenuItem.Text = "Показывать сетку";
            this.показыватьСеткуToolStripMenuItem.Click += new System.EventHandler(this.показыватьСеткуToolStripMenuItem_Click);
            // 
            // картаToolStripMenuItem
            // 
            this.картаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьНаборТайтловToolStripMenuItem});
            this.картаToolStripMenuItem.Name = "картаToolStripMenuItem";
            this.картаToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.картаToolStripMenuItem.Text = "Карта";
            // 
            // создатьНаборТайтловToolStripMenuItem
            // 
            this.создатьНаборТайтловToolStripMenuItem.Name = "создатьНаборТайтловToolStripMenuItem";
            this.создатьНаборТайтловToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.создатьНаборТайтловToolStripMenuItem.Text = "Создать Набор Тайлов";
            this.создатьНаборТайтловToolStripMenuItem.Click += new System.EventHandler(this.создатьНаборТайтловToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(551, 586);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(659, 586);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.button2_KeyDown);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewMap,
            this.OpenMap,
            this.SaveMap,
            this.toolStripSeparator1,
            this.Stamp,
            this.Fill,
            this.Choose,
            this.Move,
            this.Delete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(870, 39);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // NewMap
            // 
            this.NewMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NewMap.Image = ((System.Drawing.Image)(resources.GetObject("NewMap.Image")));
            this.NewMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewMap.Name = "NewMap";
            this.NewMap.Size = new System.Drawing.Size(36, 36);
            this.NewMap.ToolTipText = "Создать";
            this.NewMap.Click += new System.EventHandler(this.NewMap_Click);
            // 
            // OpenMap
            // 
            this.OpenMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OpenMap.Image = ((System.Drawing.Image)(resources.GetObject("OpenMap.Image")));
            this.OpenMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenMap.Name = "OpenMap";
            this.OpenMap.Size = new System.Drawing.Size(36, 36);
            this.OpenMap.ToolTipText = "Открыть";
            // 
            // SaveMap
            // 
            this.SaveMap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveMap.Image = ((System.Drawing.Image)(resources.GetObject("SaveMap.Image")));
            this.SaveMap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveMap.Name = "SaveMap";
            this.SaveMap.Size = new System.Drawing.Size(36, 36);
            this.SaveMap.ToolTipText = "Сохранить";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // Stamp
            // 
            this.Stamp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Stamp.Image = ((System.Drawing.Image)(resources.GetObject("Stamp.Image")));
            this.Stamp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Stamp.Name = "Stamp";
            this.Stamp.Size = new System.Drawing.Size(36, 36);
            this.Stamp.ToolTipText = "Штамп";
            this.Stamp.Click += new System.EventHandler(this.Stamp_Click);
            // 
            // Fill
            // 
            this.Fill.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Fill.Image = ((System.Drawing.Image)(resources.GetObject("Fill.Image")));
            this.Fill.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Fill.Name = "Fill";
            this.Fill.Size = new System.Drawing.Size(36, 36);
            this.Fill.ToolTipText = "Заливка";
            // 
            // Choose
            // 
            this.Choose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Choose.Image = ((System.Drawing.Image)(resources.GetObject("Choose.Image")));
            this.Choose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Choose.Name = "Choose";
            this.Choose.Size = new System.Drawing.Size(36, 36);
            this.Choose.ToolTipText = "Выделение";
            // 
            // Move
            // 
            this.Move.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Move.Image = ((System.Drawing.Image)(resources.GetObject("Move.Image")));
            this.Move.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Move.Name = "Move";
            this.Move.Size = new System.Drawing.Size(36, 36);
            this.Move.ToolTipText = "Перемещение";
            this.Move.Click += new System.EventHandler(this.Move_Click);
            // 
            // Delete
            // 
            this.Delete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Delete.Image = ((System.Drawing.Image)(resources.GetObject("Delete.Image")));
            this.Delete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(36, 36);
            this.Delete.ToolTipText = "Ластик";
            // 
            // layers1
            // 
            this.layers1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.layers1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.layers1.Location = new System.Drawing.Point(672, 72);
            this.layers1.Name = "layers1";
            this.layers1.Size = new System.Drawing.Size(186, 466);
            this.layers1.TabIndex = 6;
            // 
            // tiledExplorer1
            // 
            this.tiledExplorer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tiledExplorer1.Location = new System.Drawing.Point(0, 72);
            this.tiledExplorer1.Name = "tiledExplorer1";
            this.tiledExplorer1.Size = new System.Drawing.Size(217, 466);
            this.tiledExplorer1.TabIndex = 5;
            // 
            // monitor1
            // 
            this.monitor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.monitor1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.monitor1.Location = new System.Drawing.Point(238, 72);
            this.monitor1.Name = "monitor1";
            this.monitor1.Size = new System.Drawing.Size(388, 466);
            this.monitor1.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 611);
            this.Controls.Add(this.layers1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tiledExplorer1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.monitor1);
            this.Controls.Add(this.button1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьКакToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button button1;
        private Monitor monitor1;
        private System.Windows.Forms.Button button2;
        private TiledExplorer tiledExplorer1;
        private Layers layers1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton NewMap;
        private System.Windows.Forms.ToolStripButton OpenMap;
        private System.Windows.Forms.ToolStripButton SaveMap;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton Stamp;
        private System.Windows.Forms.ToolStripButton Fill;
        private System.Windows.Forms.ToolStripButton Choose;
        private System.Windows.Forms.ToolStripButton Move;
        private System.Windows.Forms.ToolStripButton Delete;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem видToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показыватьСеткуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem картаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьНаборТайтловToolStripMenuItem;
    }
}

