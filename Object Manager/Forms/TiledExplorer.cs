﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ObjectManager
{
	public partial class TiledExplorer: UserControl
	{
        public bool FileIsOpen;
        Point Selected = new Point(-1,-1);


        List<Tiles> TileList;
        Point GreedSize;
        PictureBox pbox;
        Graphics graphics;
        Bitmap bitmap;

        public TiledExplorer()
        {
            InitializeComponent();
            BaseHelper.SelectedTile = new VChunk[0, 0];
            TileList = new List<Tiles>();
            ResHelper.TileImage = new List<Image>();
        }


        public void AddTile(string Patch, string name)
        {
            int i =ResHelper.TileImage.Count;
            //if(i < 0)
            //    i = 0;
            LoadTile(Patch, i);

            Tiles tt = new Tiles();
            GenerateNewTileGrid(ref tt.Tile, i);
            tt.Size = new ObjectManager.Size(tt.Tile.GetLength(0), tt.Tile.GetLength(1));

            TileList.Add(tt);

            

            name = name.Substring(0, name.IndexOf("."));
            comboBox1.Items.Add(name);
            comboBox1.SelectedIndex = i;


            //ResHelper.SelectedTileImage = i;

            Draw();
        }



        public void LoadTile(string PatchToFile, int index)
        {
            ResHelper.TileImage.Add(Image.FromFile(PatchToFile));
            pbox = (PictureBox)panel1.Controls[0].Controls[0];



            ResizePictureBox(index);
        }

        void ResizePictureBox(int index)
        {
            bitmap = new Bitmap(ResHelper.TileImage[index].Width, ResHelper.TileImage[index].Height);
            graphics = Graphics.FromImage(bitmap);
            pbox.Height = ResHelper.TileImage[index].Height;
            pbox.Width = ResHelper.TileImage[index].Width;
        }



        void GenerateNewTileGrid(ref Tile[,] tile, int index)
        {

            GreedSize.X = ResHelper.TileImage[index].Width / MapHelper.ChunkSize.X;
            GreedSize.Y = ResHelper.TileImage[index].Height / MapHelper.ChunkSize.Y;

            tile = new Tile[GreedSize.X, GreedSize.Y];

            Point p = new Point();
            for (int i = 0; i < GreedSize.Y; i++)
            {
                for (int n = 0; n < GreedSize.X; n++)
                {
                    tile[n, i] = new Tile();

                    tile[n, i].Location = p;
                    p.X += MapHelper.ChunkSize.X;
                }

                p.X = 0;
                p.Y += MapHelper.ChunkSize.Y;
            }
        }


        Pen blackPen = new Pen(Color.Black, 1);
        void ViewGrid()
        {
            for (int i = 0; i < GreedSize.Y; i++)
            {
                for (int n = 0; n < GreedSize.X; n++)
                {
                    graphics.DrawRectangle(blackPen, TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.X, TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.Y, MapHelper.ChunkSize.X, MapHelper.ChunkSize.Y);
                    if (TileList[ResHelper.SelectedTileImage].Tile[n, i].Selected)
                        graphics.DrawImage(ResHelper.SelectedTile, TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.X, TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.Y, ResHelper.SelectedTile.Width, ResHelper.SelectedTile.Height); ;
                }

            }

            PBReflesh();
        }


        void PBReflesh()
        {
            pbox.Image = bitmap;
            pbox.Refresh();
        }


#region Mouse

        bool LeftMouseDown = false;
        bool CreateRect = false;
        Point MousePoint;

        private void Tile_MouseDown(object sender, MouseEventArgs e)
        {
            if (ResHelper.SelectedTileImage != (-1))
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    LeftMouseDown = true;
                    MousePoint = e.Location;
                }


        }

        private void Tile_MouseUp(object sender, MouseEventArgs e)
        {
            if (ResHelper.SelectedTileImage != (-1))
            {

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    LeftMouseDown = false;
                    if (CreateRect)
                    {
                        //TileRectangle = new Rectangle();
                        //DrawTileOnly();
                        if (Fixate)
                        {
                            Fixate = false;
                            LeftMouseDown = false;
                            ClearAllSelect();
                            Draw();
                            BaseHelper.SelectedTile = new VChunk[0, 0];
                        }
                        CreateRect = false;
                    }
                    else
                    {
                        CreateRect = true;
                        //Добавить отправку 1 куска на карту
                        if (!MultSel)
                        {
                            BaseHelper.SelectedTile = new VChunk[1, 1];
                            BaseHelper.SelectedTile[0, 0] = new VChunk();
                            BaseHelper.SelectedTile[0, 0].Position = TileList[ResHelper.SelectedTileImage].Tile[TempPoint.X, TempPoint.Y].Location;
                        }
                        else
                        {
                            int ii, nn;
                            ii = nn = 0;
                            BaseHelper.SelectedTile = new VChunk[(Selected.X - TempPoint.X) + 1, (Selected.Y - TempPoint.Y) + 1];

                            for (int i = TempPoint.Y; i < Selected.Y + 1; i++)
                            {
                                for (int n = TempPoint.X; n < Selected.X + 1; n++)
                                {

                                    //Здесь запись кусков
                                    BaseHelper.SelectedTile[nn, ii] = new VChunk();
                                    BaseHelper.SelectedTile[nn, ii].Index.X = n;
                                    BaseHelper.SelectedTile[nn, ii].Index.Y = i;
                                    BaseHelper.SelectedTile[nn, ii].Position = TileList[ResHelper.SelectedTileImage].Tile[n, i].Location;

                                    nn += 1;
                                }
                                nn = 0;
                                ii += 1;
                            }
                        }
                    }
                }

            }


        }

        bool Fixate = false;
        bool MultSel;
        Point TempPoint = new Point();
        private void Tile_MouseMove(object sender, MouseEventArgs e)
        {
            if (ResHelper.SelectedTileImage != (-1))
            {
                if (!Fixate)
                {
                    for (int i = 0; i < GreedSize.Y; i++)
                    {
                        for (int n = 0; n < GreedSize.X; n++)
                        {
                            if (TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.X < e.X & e.X < TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.X + MapHelper.ChunkSize.X)
                                if (TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.Y < e.Y & e.Y < TileList[ResHelper.SelectedTileImage].Tile[n, i].Location.Y + MapHelper.ChunkSize.Y)
                                {
                                    if (Selected.X != n || Selected.Y != i)
                                    {

                                        Selected.X = n;
                                        Selected.Y = i;

                                        if (CreateRect)
                                        {
                                            //Здесь добавить код для отправки кусков на карту
                                            Fixate = true;
                                            //CreateRect = false;



                                            return;
                                        }

                                        if (!LeftMouseDown)
                                        {
                                            ClearAllSelect();
                                            TileList[ResHelper.SelectedTileImage].Tile[n, i].Selected = true;
                                            TempPoint.X = Selected.X;
                                            TempPoint.Y = Selected.Y;

                                            //if (CreateRect)

                                            MultSel = false;

                                        }
                                        else
                                        {
                                            ClearAllSelect();
                                            MultiSelect(TempPoint, Selected);
                                            MultSel = true;
                                        }


                                        Draw();
                                    }
                                }

                        }
                    }


                }
            }
        }



        private void Tile_MouseClick(object sender, MouseEventArgs e)
        {

        }

#endregion

        void MultiSelect(Point p1, Point p2)
        {


            for (int i = p1.Y; i < p2.Y + 1; i++)
            {
                for (int n = p1.X; n < p2.X + 1; n++)
                {
                    TileList[ResHelper.SelectedTileImage].Tile[n, i].Selected = true;

                }
            }

        }


        void ClearAllSelect()
        {
            for (int i = 0; i < GreedSize.Y; i++)
            {
                for (int n = 0; n < GreedSize.X; n++)
                {
                    if (TileList[ResHelper.SelectedTileImage].Tile[n, i].Selected)
                        TileList[ResHelper.SelectedTileImage].Tile[n, i].Selected = false;
                }
            }
        }

        void DrawImage()
        {
            graphics.Clear(Color.White);
            graphics.DrawImage(ResHelper.TileImage[ResHelper.SelectedTileImage], Point.Empty);
        }


        void Draw()
        {
            DrawImage();
            ViewGrid();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            ResHelper.SelectedTileImage = ((ComboBox)sender).SelectedIndex;
            GreedSize.X = TileList[ResHelper.SelectedTileImage].Size.Width;
            GreedSize.Y = TileList[ResHelper.SelectedTileImage].Size.Height;
            ResizePictureBox(ResHelper.SelectedTileImage);

            Draw();
        }


	}

    public class Tiles
    {
        public Tile[,] Tile;
        public Size Size;
    }

    public class Tile
    {
        public Point Location;
        public bool Selected;
    }

    public class Size
    {
        public int Width;
        public int Height;

        public Size() { }
        public Size(int Width, int Height)
        {
            this.Width = Width;
            this.Height = Height;
        }
    }
}
