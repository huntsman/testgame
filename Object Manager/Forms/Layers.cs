﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ObjectManager
{
    public partial class Layers : UserControl
    {
        public Layers()
        {
            InitializeComponent();
            //LoadImages();
        }

        void LoadImages()
        {
            NewLayer.Image = Image.FromFile("./Images/NewLayer.bmp");
            UpLayer.Image = Image.FromFile("./Images/UpLayer.bmp");
            DownLayer.Image = Image.FromFile("./Images/DownLayer.bmp");
            DeleteLayer.Image = Image.FromFile("./Images/DeleteLayer.bmp");
        }
    }
}
