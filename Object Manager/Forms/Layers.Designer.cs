﻿namespace ObjectManager
{
    partial class Layers
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.NewLayer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.UpLayer = new System.Windows.Forms.Button();
            this.DownLayer = new System.Windows.Forms.Button();
            this.DeleteLayer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(1, 16);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(176, 184);
            this.checkedListBox1.TabIndex = 0;
            // 
            // NewLayer
            // 
            this.NewLayer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.NewLayer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.NewLayer.Location = new System.Drawing.Point(6, 254);
            this.NewLayer.Name = "NewLayer";
            this.NewLayer.Size = new System.Drawing.Size(25, 25);
            this.NewLayer.TabIndex = 1;
            this.NewLayer.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Слои:";
            // 
            // UpLayer
            // 
            this.UpLayer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.UpLayer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.UpLayer.Location = new System.Drawing.Point(37, 254);
            this.UpLayer.Name = "UpLayer";
            this.UpLayer.Size = new System.Drawing.Size(25, 25);
            this.UpLayer.TabIndex = 1;
            this.UpLayer.UseVisualStyleBackColor = true;
            // 
            // DownLayer
            // 
            this.DownLayer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DownLayer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DownLayer.Location = new System.Drawing.Point(68, 254);
            this.DownLayer.Name = "DownLayer";
            this.DownLayer.Size = new System.Drawing.Size(25, 25);
            this.DownLayer.TabIndex = 1;
            this.DownLayer.UseVisualStyleBackColor = true;
            // 
            // DeleteLayer
            // 
            this.DeleteLayer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteLayer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DeleteLayer.Location = new System.Drawing.Point(99, 254);
            this.DeleteLayer.Name = "DeleteLayer";
            this.DeleteLayer.Size = new System.Drawing.Size(25, 25);
            this.DeleteLayer.TabIndex = 1;
            this.DeleteLayer.UseVisualStyleBackColor = true;
            // 
            // Layers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeleteLayer);
            this.Controls.Add(this.DownLayer);
            this.Controls.Add(this.UpLayer);
            this.Controls.Add(this.NewLayer);
            this.Controls.Add(this.checkedListBox1);
            this.Name = "Layers";
            this.Size = new System.Drawing.Size(182, 289);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button NewLayer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button UpLayer;
        private System.Windows.Forms.Button DownLayer;
        private System.Windows.Forms.Button DeleteLayer;
    }
}
