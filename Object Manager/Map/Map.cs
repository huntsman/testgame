﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ObjectManager
{
    public class Map
    {
        public Viewer viewer;
        ZoneGenerator ZG;
        Zone zone;
        int ii, nn;
        Point p;
        public bool GridView;
        public Map()
        {
            viewer = new Viewer(5, 5);

            ZG = new ZoneGenerator();
            zone = ZG.Generate();


            MapHelper.MapSize = new Point(zone.Chunks.GetLength(0), zone.Chunks.GetLength(1));

            //Заполняем Viewer
            ii = nn = 0;
            for (int i = 0; i < viewer.Height; i++)
            {
                for (int n = 0; n < viewer.Width; n++)
                {
                    p.X = nn * MapHelper.ChunkSize.X + viewer.Shift.X;
                    p.Y = ii * MapHelper.ChunkSize.Y + viewer.Shift.Y;


                    viewer.vchunk[nn, ii] = new VChunk();
                    viewer.vchunk[nn, ii].Index = new Point(n, i);
                    viewer.vchunk[nn, ii].Position = p;

                    //DrawHelper.monitor.Draw(zone.MapImage, p, zone.Chunks[n, i].ImageBlock);
                    nn += 1;
                }

                nn = 0;
                ii += 1;
            }
        }

        //int ii, nn;
        //Pen blackPen = new Pen(Color.Black, 10);
        //Point p;
        //Point sdv = new Point((int)(-(64 * 1.34)), (int)(-(64 * 1.34)));
        Rectangle trect = new Rectangle();
        //Point ptt;
        public void Draw()
        {

            for (int i = 0; i < viewer.Height; i++)
            {
                for (int n = 0; n < viewer.Width; n++)
                {
                    p.X = viewer.Shift.X + viewer.vchunk[n, i].Position.X;
                    p.Y = viewer.Shift.Y + viewer.vchunk[n, i].Position.Y;
                    if (zone.Chunks[viewer.vchunk[n, i].Index.X, viewer.vchunk[n, i].Index.Y].TileImageIndex >= 0)
                        DrawHelper.monitor.DrawImage(ResHelper.TileImage[zone.Chunks[viewer.vchunk[n, i].Index.X, viewer.vchunk[n, i].Index.Y].TileImageIndex], p, zone.Chunks[viewer.vchunk[n, i].Index.X, viewer.vchunk[n, i].Index.Y].ImageBlock);

                    //if (viewer.Selected.X == n & viewer.Selected.Y == i)
                    //{
                    //    ptt = viewer.Selected;

                    //}

                }


            }

            if (viewer.Selected.X != (-1))
                DrawNewChunk();

            if (GridView)
                ViewGrid();
        }


        void DrawNewChunk()
        {
            if (viewer.Selected.X != (-1))
            {

                p.X = viewer.Shift.X + viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Position.X;
                p.Y = viewer.Shift.Y + viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Position.Y;

                if (BaseHelper.SelectedTile.Length == 0)
                    DrawHelper.monitor.DrawImage(ResHelper.SelectedTile, p);
                else if (BaseHelper.SelectedTile.Length == 1)
                {
                    trect.X = BaseHelper.SelectedTile[0, 0].Position.X;
                    trect.Y = BaseHelper.SelectedTile[0, 0].Position.Y;
                    trect.Width = MapHelper.ChunkSize.X;
                    trect.Height = MapHelper.ChunkSize.Y;
                    DrawHelper.monitor.DrawImage(ResHelper.TileImage[ResHelper.SelectedTileImage], p, trect);
                }
                else
                {
                    for (int i = 0; i < BaseHelper.SelectedTile.GetLength(1); i++)
                    {
                        for (int n = 0; n < BaseHelper.SelectedTile.GetLength(0); n++)
                        {
                            if(viewer.Selected.X + n < viewer.Width &  viewer.Selected.Y + i < viewer.Height)
                            {
                            p.X = viewer.Shift.X + viewer.vchunk[viewer.Selected.X + n, viewer.Selected.Y + i].Position.X;
                            p.Y = viewer.Shift.Y + viewer.vchunk[viewer.Selected.X + n, viewer.Selected.Y + i].Position.Y;

                            trect.X = BaseHelper.SelectedTile[n, i].Position.X;
                            trect.Y = BaseHelper.SelectedTile[n, i].Position.Y;
                            trect.Width = MapHelper.ChunkSize.X;
                            trect.Height = MapHelper.ChunkSize.Y;
                            DrawHelper.monitor.DrawImage(ResHelper.TileImage[ResHelper.SelectedTileImage], p, trect);
                            }

                        }
                    }

                }
            }

        }


        public void AddNewTile()
        {
            if (BaseHelper.SelectedTile.Length == 1)
            {
                zone.Chunks[viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Index.X, viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Index.Y].ImageBlock.X = BaseHelper.SelectedTile[0, 0].Position.X;
                zone.Chunks[viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Index.X, viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Index.Y].ImageBlock.Y = BaseHelper.SelectedTile[0, 0].Position.Y;
            }
            else
            {

                int tx = viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Index.X;
                int txt = tx;
                int ty = viewer.vchunk[viewer.Selected.X, viewer.Selected.Y].Index.Y;
                for (int i = 0; i < BaseHelper.SelectedTile.GetLength(1); i++)
                {
                    for (int n = 0; n < BaseHelper.SelectedTile.GetLength(0); n++)
                    {
                        if (tx < MapHelper.MapSize.X & ty < MapHelper.MapSize.Y)
                        {

                            zone.Chunks[txt, ty].ImageBlock.X = BaseHelper.SelectedTile[n, i].Position.X;
                            zone.Chunks[txt, ty].ImageBlock.Y = BaseHelper.SelectedTile[n, i].Position.Y;


                            txt += 1;
                        }

                        txt = tx;
                        ty += 1;
                    }
                }
            }
        }


        Pen blackPe = new Pen(Color.Black, 1);
        Point ts = new Point(MapHelper.ChunkSize.X, MapHelper.ChunkSize.Y);
        void ViewGrid()
        {
            int xpr = 0;
            int ypr = 0;
            
            for (int i = 0; i < viewer.Width; i++)
            {
                for (int n = 0; n < viewer.Height; n++)
                {
                    DrawHelper.monitor.DrawRectangle(blackPe, viewer.Shift.X + xpr /*+ sdv.X*/, viewer.Shift.Y + ypr /*+ sdv.Y*/, ts.X, ts.Y);
                    
                    ypr += ts.Y;

                }
                xpr += ts.X;


                ypr = 0;
            }
        }




    }
}
