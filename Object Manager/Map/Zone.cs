﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ObjectManager
{
    class Zone
    {
        public string Name;
        public Image MapImage;
        public Chunk[,] Chunks;


        public Zone(string name, Image image)
        {
            Name = name;
            MapImage = image;
        }


    }
}
