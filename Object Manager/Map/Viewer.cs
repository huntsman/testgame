﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ObjectManager
{
    public class Viewer
    {

        private Point ViewPoint;
        public int Width, Height;
        public Point Selected;

        public Viewer(int Width, int Height)
        {
            Selected = new Point(-1,-1);
            this.Width = Width;
            this.Height = Height;
            vchunk = new VChunk[Width, Height];
        }

        public VChunk[,] vchunk;

        private Point shift = new Point();


        bool freex;
        bool freey;
        public Point Shift
        {
            get { return shift; }
            set
            {
                shift = value;

                if (shift.X == 0)
                    freex = false;
                if (shift.Y == 0)
                    freey = false;

                if (!freex)
                {
                    if (shift.X > MapHelper.ChunkSize.X)
                    {
                        if(ShiftX(-1))
                           shift.X = 0;
                    }

                    if (shift.X < (-MapHelper.ChunkSize.X))
                    {
                        if(ShiftX(1))
                           shift.X = 0;
                        
                    }
                }

                if (!freey)
                {
                    if (shift.Y > MapHelper.ChunkSize.Y)
                    {
                        if (ShiftY(-1))
                            shift.Y = 0;
                    }

                    if (shift.Y < (-MapHelper.ChunkSize.Y))
                    {
                        if (ShiftY(1))
                            shift.Y = 0;

                    }
                }





            }
        }

        bool ShiftX(int x)
        {
            ViewPoint.X += x;

            if (ViewPoint.X >= 0 & ViewPoint.X < MapHelper.MapSize.X - Width + 1)
            {

                for (int i = 0; i < Height; i++)
                {
                    for (int n = 0; n < Width; n++)
                    {
                        vchunk[n, i].Index.X += x;

                    }
                }
            }

            if (ViewPoint.X < 0 || ViewPoint.X > MapHelper.MapSize.X - Width)
            {
                ViewPoint.X += x * (-1);
                freex = true;
                return false;
            }

            return true;
        }

        bool ShiftY(int y)
        {
            ViewPoint.Y += y;

            if (ViewPoint.Y >= 0 & ViewPoint.Y < MapHelper.MapSize.Y - Height + 1)
            {

                for (int i = 0; i < Height; i++)
                {
                    for (int n = 0; n < Width; n++)
                    {
                        vchunk[n, i].Index.Y += y;

                    }
                }
            }

            if (ViewPoint.Y < 0 || ViewPoint.Y > MapHelper.MapSize.Y - Height)
            {
                ViewPoint.Y += y * (-1);
                freey = true;
                return false;
            }

            return true;
        }


        public void AddToViewer()
        {

        }


        //public void SetViewPoint(Point point)
        //{

        //    ViewPoint.X = point.X / MapHelper.ChunkSize.X;
        //    ViewPoint.Y = point.Y / MapHelper.ChunkSize.Y;

        //}


    }

    public class VChunk
    {
        public Point Index;
        public Point Position;


    }
}
